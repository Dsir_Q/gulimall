package com.atguigu.gulimall.gulimalllware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GulimalllWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimalllWareApplication.class, args);
    }

}
